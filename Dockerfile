FROM python:3.10-alpine3.18 AS build

RUN apk --no-cache add libxml2-dev libxslt-dev gcc python3 python3-dev py3-pip py3-lxml musl-dev linux-headers

RUN python3 -m ensurepip --upgrade
RUN mkdir /source
COPY requirements.txt /source/
RUN pip install -r /source/requirements.txt

FROM python:3.10-alpine3.18 AS final

RUN apk upgrade --no-cache
RUN mkdir /dsvw
RUN adduser -D dsvw && chown -R dsvw:dsvw /dsvw

COPY dsvw.py .
RUN sed -i 's/127.0.0.1/0.0.0.0/g' dsvw.py
RUN pip3 install lxml
COPY --from=build /source /

EXPOSE 65412
USER dsvw
CMD ["python3", "/dsvw.py"]